/*require.config({

    shim:{
        'testc': {
            deps: ['https://cdn.bootcss.com/ace/1.2.6/ace.js','https://cdn.bootcss.com/marked/0.3.12/marked.js'],
            // 模块外部调用时的名称， 传入形参调用。 也可以不写exports， 调用的时候直接写代码
            //exports: 'marked'
        },
        'testA':{
            deps: ['https://cdn.bootcss.com/marked/0.3.12/marked.js'],
            exports: 'marked'
        }
    },
    paths: {
        "jquery": "lib/jquery.min",
        'testc':'C',
        'testA':'A',
        'marked':'https://cdn.bootcss.com/marked/0.3.12/marked'
    },
    //waitSeconds: 0 //解决加载超时
});*/

/*

require(['jquery','testc','marked'], function ($,testc,marked){
    //$('body').append('<h1>123</h1>');

    /!*var result=test();
     console.info(result);*!/

    //test();

    //console.info(c123.fun3('editor1',$('#editor1').text()));


    //console.info(testc.fun_testace('editor1',$('#editor1').text()));

    //console.info(c123.fun2())


    //console.log(testa.add(marked));


    //console.info(marked('111111111111111111111'))

    //调用普通函数
    //testA();
    //ddd();


});
*/


/*
require.config({
    shim:{
        'A':{
            deps:['ace'],
            exports:'A'
        }
    },
    paths:{
        'marked':'https://cdn.bootcss.com/marked/0.3.12/marked',
        'ace':'https://cdn.bootcss.com/ace/1.2.6/ace'
    }
});
require(['A'], function (A){

    console.info(A.test_marked('# this is a test.'));

    A.test_ace('editor1','11111111111111111111');
});
*/


/*marked, 需要传形参
ace, 不符合AMD规范的需要 shim*/


require.config({
    shim:{
        'A':{
            deps:['marked','ace'],
            exports:'A'
        }
    },
    paths:{
        'marked':'https://cdn.bootcss.com/marked/0.3.12/marked',
        'ace':'https://cdn.bootcss.com/ace/1.2.6/ace',
        'A':'A',
        'C':'C'
    }
});

require(['A','C'], function (A,C){

    A.test_ace('editor1','11111111111111111111');

    A.test();

    //A.test_marked();

    C.test_marked();


});






/*require.config({
    shim:{
        c:{
            deps: ['https://cdn.bootcss.com/ace/1.2.6/ace.js']
        }
    },
    paths:{
        'c':'c'
    }
})
require(['c'], function (){

    ace.edit('editor1','11111111111111111111');

    //c123.fun3('editor1','11111111111111111111');

    //c.fun_testace('editor1','11111111111111111111');

});*/
