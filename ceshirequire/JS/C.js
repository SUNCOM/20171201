define(['marked','ace'],function(marked,ace){
    return {
        test_marked: function (){
            return marked('# 111111111');
        },
        test_ace:function(ID,Content){
            var EDITOR = ace.edit(ID);
            EDITOR.setFontSize(20);
            EDITOR.session.setMode("ace/mode/markdown");
            EDITOR.setOption("wrap", "free");//自动换行
            EDITOR.setShowPrintMargin(false);
            EDITOR.setOptions({
                enableBasicAutocompletion: true,
                enableSnippets: true,
                enableLiveAutocompletion: true,
                useSoftTabs: true
            });
            if (Content != undefined && Content.length)
                EDITOR.setValue(Content);
        }
    };
});